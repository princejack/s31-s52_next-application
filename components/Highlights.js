import React from 'react';
import { Row, Col } from 'react-bootstrap';//acquire the bootstrap grid system.
import Card from 'react-bootstrap/Card';//acquire the Card Component from react bootstrap

// create a Highlights()

export default function Highlights(){
	return(
			<Row>
				<Col>
					<Card className="cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Learn from Home</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum et dolorem quos fuga quam enim accusantium, quae suscipit perferendis, quidem magnam. Possimus vitae ad illum laboriosam tempora nulla fuga placeat!
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				
				<Col>
					<Card className="cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum et dolorem quos fuga quam enim accusantium, quae suscipit perferendis, quidem magnam. Possimus vitae ad illum laboriosam tempora nulla fuga placeat!
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col>
					<Card className="cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum et dolorem quos fuga quam enim accusantium, quae suscipit perferendis, quidem magnam. Possimus vitae ad illum laboriosam tempora nulla fuga placeat!
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

			
			</Row>
		)
}
