import React from 'react'
import { Jumbotron, Row, Col } from 'react-bootstrap'
import Link from 'next/Link' //lets acquire a third component from next dependency

//lets create a function that will allow us to create a Banner component.
export default function Banner(){
	return (
			<Row>
				<Col>
					<Jumbotron>
						<h1>WELCOME TO MY NEXT APP</h1>
						<p>This is my first Next Application Project!</p>
						<Link href="/">
							<a>Go to Desired Destination</a>
						</Link>
					</Jumbotron>
				</Col>
			</Row>
		)
}

//S33-S53 Next-Application
//commit -m "added the needed components to rebuild the Next Project"