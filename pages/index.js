import Head from 'next/head'
import Banner from '../components/Banner'//acquire the components we want to see
import Highlights from '../components/Highlights'//acquire the components we want to see


//create a function called Home
export default function Home() {
  return(
    <>
      <Banner/>
      <Highlights/>
    </>
    )

}